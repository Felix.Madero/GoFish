from random import *

def main():
    #inputToCardNumberTest()
    #return
    playerHasWon = False
    lastCardDrawn = ""
    turn = 0
    playerHand = []
    cpuHand = []

    playerBooks = []
    cpuBooks = []


    # Stat tracking
    playerCardsHeldLongest = {}
    cpuCardsHeldLongest = {}

    playerNumTimesGuessed = {} # keys 1 - 13
    cpuNumTimesGuessed = {}

    timesChangedHands = {}

    playerNumCorrectGuesses = 0
    cpuNumCorrectGuesses = 0

    playerNumGuesses = 0
    cpuNumGuesses = 0

    cardsPerTransfer = []
    
    
    
    #Ask user to select CPU mode
    # By Felix
    print("************************************************************")
    print(".            \n\
           .:/ \n\
.      ,,///;,   ,;/ \n\
  .   o:::::::;;///         G O  F I S H \n\
     >::::::::;;\\\    by David, Mike, and Felix\n\
       ''\\\\\'\" ';\ \n\
          ';\ ")
    print("Collect as many cards from the CPU as possible by guessing what they have in their hand. Good luck!")
    print("What kind of computer player would you like to play against?")
    print("Press 1 for Simple Mode")
    print("Press 2 for Smart Mode")
    print("Press 3 for Devious Mode")

    while True:

        try:
            mode = input("Your selection: ")
            mode = int(mode)
            

        except ValueError:
            print("That is not a valid selection")
            continue
        else:
            if(mode < 1 or mode > 3):
                print("That is not a valid selection")
                continue
            else:
                break

    
    aces = ["1a", "1b", "1c", "1d"]
    twos = ["2a", "2b", "2c", "2d"]
    threes = ["3a", "3b", "3c", "3d"]
    fours = ["4a", "4b", "4c", "4d"]
    fives = ["5a", "5b", "5c", "5d"]
    sixes = ["6a", "6b", "6c", "6d"]
    sevens = ["7a", "7b", "7c", "7d"]
    eights = ["8a", "8b", "8c", "8d"]
    nines = ["9a", "9b", "9c", "9d"]
    tens = ["10a", "10b", "10c", "10d"]
    jacks = ["11a", "11b", "11c", "11d"]
    queens = ["12a", "12b", "12c", "12d"]
    kings = ["13a", "13b", "13c", "13d"]
    books = [aces, twos, threes, fours, fives, sixes, sevens, eights, nines, tens, jacks, queens, kings]
    fullDeck = aces + twos + threes + fours + fives + sixes + sevens + eights + nines + tens + jacks + queens + kings
    while (playerHasWon == False):
        cardsToAdd = []
        if turn == 0: # Game start. Deal out 7 cards to each player
            for x in range(0, 7):
                drawCard(cpuHand, fullDeck)
                drawCard(playerHand, fullDeck)
                
# AI Logic by Mike
# Turn mechanics by Felix
# Input and Transfer functions by David 
        # Player Turn
        
        if turn % 2 == 0:
            requestedRank = 0
            if len(playerHand) == 0:
                drawnCard = drawCard(playerHand, fullDeck)
                print("You: drew a " + formatCardForPrinting(drawnCard))
            else:
                #If user entered mode 3
                if(mode == 3):
                    # AI Logic by Mike
                    #Every 3rd turn of the CPU, enter the devious mode
                    if turn % 3 == 0: 
                    # (len(fullDeck) % 4) == 2:
                    #if (len(fullDeck)==34 or len(fullDeck)==30 or len(fullDeck)==26 or len(fullDeck)==22 or len(fullDeck)==18 or len(fullDeck)==14 or len(fullDeck)==10 or len(fullDeck)==6 or len(fullDeck)==2): # Go fish
                        print("\nIt's your turn")
                        #print("CPU is cheating!")
                        if len(playerBooks) > 0:
                            print("Your " + printBooks(playerBooks))
                        if len(cpuBooks) > 0:
                            print("CPU " + printBooks(cpuBooks))
                        #print("CPU hand: " + str(cpuHand)) # For testing
                        requestedRank = inputCardRequest(playerHand) # User inputs rank of card
                        #Don't give any cards to user, even if the inputCard is correct
                        cardsToAdd = []
                        #Tell the user to go fish
                        print("CPU: Go fish")

                    #For every 1st and 2nd turn, CPU should be in regular mode
                    else: # Add new cards to hand, separate out books
                        print("\nIt's your turn")
                        if len(playerBooks) > 0:
                            print("Your " + printBooks(playerBooks))
                        if len(cpuBooks) > 0:
                            print("CPU " + printBooks(cpuBooks))
                        #print("Your hand: " + str(playerHand))
                        #print("CPU hand: " + str(cpuHand)) # For testing
                        requestedRank = inputCardRequest(playerHand) # User inputs rank of card
                        cardsToAdd = transferCardsFromPlayer(requestedRank, cpuHand) # Get card from other player
                        print("CPU had " + str(len(cardsToAdd)) + " " + formatCardForPrinting(requestedRank + ' ') + "s")
                        playerHand = playerHand + cardsToAdd
                        
                    if len(fullDeck) != 0:
                        drawnCard = drawCard(playerHand, fullDeck)
                        print("You: drew a " + formatCardForPrinting(drawnCard))


                else: # CPU is NOT in devious mode
                    print("\nIt's your turn")
                    if len(playerBooks) > 0:
                        print("Your " + printBooks(playerBooks))
                    if len(cpuBooks) > 0:
                        print("CPU " + printBooks(cpuBooks))
                    #print("CPU hand: " + str(cpuHand)) # For testing 
                    requestedRank = inputCardRequest(playerHand) # User inputs rank of card

                    cardsToAdd = transferCardsFromPlayer(requestedRank, cpuHand) # Get card from other player
                    if (len(cardsToAdd) == 0): # Go fish
                        print("CPU: Go fish")
                    else: # Add new cards to hand, separate out books
                        print("CPU had " + str(len(cardsToAdd)) + " " + formatCardForPrinting(requestedRank + ' ') + "s")

                        playerHand = playerHand + cardsToAdd
                        #separateBooks(playerHand, books, playerBooks)
                    if len(fullDeck) != 0:
                        drawnCard = drawCard(playerHand, fullDeck)
                        print("You: drew a " + formatCardForPrinting(drawnCard))
                separateBooks(playerHand, books, playerBooks)

                # Add stats
                if requestedRank in playerNumTimesGuessed:
                    playerNumTimesGuessed[requestedRank] += 1
                else:
                    playerNumTimesGuessed[requestedRank] = 1
                playerNumGuesses += 1
                if len(cardsToAdd) > 0:
                    playerNumCorrectGuesses += 1
                    cardsPerTransfer.append(len(cardsToAdd))
                for card in cardsToAdd:
                    if card in timesChangedHands:
                        timesChangedHands[card] += 1
                    else:
                        timesChangedHands[card] = 1
                for card in playerHand:
                    if card in playerCardsHeldLongest:
                        playerCardsHeldLongest[card] += 1
                    else:
                        playerCardsHeldLongest[card] = 1
            


        # CPU turn
        if turn % 2 == 1:
            if len(cpuHand) == 0:
                drawnCard = drawCard(cpuHand, fullDeck)
                countDrawn = drawnCard
                #print("CPU: Drew a " + drawnCard)

            #If the mode is 1 or 3, enter
            if(mode == 1 or mode == 3):
                
                print("\nCPU Turn")
                #generate random integer based on the length of the CPUs hand
                randomCard = randint(0, (len(cpuHand) - 1))

                #if the length of the card is equal to 3, i.e. 10d or 11c
                if len(cpuHand[randomCard])==3:
                    #parse the CPUs card to 2, so that only 10 or 11 is selected
                    parseCard = cpuHand[randomCard]
                    parsedCard = parseCard[:2]
                else:
                    #parse the CPUs card to 1, so that 1c or 4d becomes 1 or 4
                    parseCard = cpuHand[randomCard]
                    parsedCard = parseCard[:1]

                #Set the requested card to the parsed Card
                rankToRequest = parsedCard
                print("CPU: Got any " + rankToRequest + "?")
                cardsToAdd = transferCardsFromPlayer(rankToRequest, playerHand)
                if len(cardsToAdd) == 0:
                    print("You: Go fish.")
                else:
                    print("You had " + str(len(cardsToAdd)) + " " + formatCardForPrinting(rankToRequest + ' ') + "s")

                cpuHand = cpuHand + cardsToAdd
                separateBooks(cpuHand, books, cpuBooks)
                
                if len(fullDeck) != 0:
                    drawnCard = drawCard(cpuHand, fullDeck)
                    #print("CPU: Drew a " + drawnCard)
                    
            #Enter mode 2
            
            elif(mode == 2):
                
                print("\nCPU Turn")
                #If its the 1st turn, meaning that the deck is of length 37 after being dealt, enter
                if len(fullDeck)==37:

                    #generate random integer based on the length of the CPUs hand
                    randomCard = randint(0, (len(cpuHand) - 1))

                    #if the length of the card is equal to 3, i.e. 10d or 11c
                    if len(cpuHand[randomCard])==3:
                        #parse the CPUs card to 2, so that only 10 or 11 is selected
                        parseCard = cpuHand[randomCard]
                        parsedCard = parseCard[:2]
                    else:
                        #parse the CPUs card to 1, so that 1c or 4d becomes 1 or 4
                        parseCard = cpuHand[randomCard]
                        parsedCard = parseCard[:1]

                    #Set the requested card to the parsed Card
                    rankToRequest = parsedCard

                #If it is not the first turn, enter
                else:
                    #if the length of the drawn card is equal to 3 parse it down to length of 2
                    if len(countDrawn)==3:
                        parseDrawn = countDrawn[:2]
                    #if the length of the drawn card is equal to 2 parse it down to lenght of 1
                    else:
                        parseDrawn = countDrawn[:1]

                    #Request the last drawn card that is parsed down
                    rankToRequest = parseDrawn
                if rankOKToGuess(rankToRequest, cpuHand, cpuBooks, playerBooks) == False:
                    randCardIndex = randint(0, (len(cpuHand) - 1))
                    rankToRequest = parseCardNum(cpuHand[randCardIndex]) 
                print("CPU: Got any " + formatCardForPrinting(rankToRequest) + "s?")
                cardsToAdd = transferCardsFromPlayer(rankToRequest, playerHand)
                if len(cardsToAdd) == 0:
                    
                    print("You: Go fish.")
                else:
                    print("You had " + str(len(cardsToAdd)) + " " + formatCardForPrinting(rankToRequest + ' ') + "s")

                cpuHand = cpuHand + cardsToAdd
                separateBooks(cpuHand, books, cpuBooks)
                
                if len(fullDeck) != 0 :
                    drawnCard = drawCard(cpuHand, fullDeck)
                    #print("CPU: Drew a " + drawnCard)
                    #set the drawnCard to countDrawn
                    countDrawn = drawnCard
            # Add stats
            if rankToRequest in cpuNumTimesGuessed:
                cpuNumTimesGuessed[rankToRequest] += 1
            else:
                cpuNumTimesGuessed[rankToRequest] = 1
            cpuNumGuesses += 1
# Stat tracking by David
            if len(cardsToAdd) > 0:
                cpuNumCorrectGuesses += 1
                cardsPerTransfer.append(len(cardsToAdd))
            for card in cardsToAdd:
                if card in timesChangedHands:
                    timesChangedHands[card] += 1
                else:
                    timesChangedHands[card] = 1
            for card in cpuHand:
                if card in cpuCardsHeldLongest:
                    cpuCardsHeldLongest[card] += 1
                else: 
                    cpuCardsHeldLongest[card] = 1
              
        
# Victory Logic by David

        if len(playerHand) == 0 and len(cpuHand) == 0:

            # Show game statistics
            
            print("**** STATS ****")
            print("Both players:")
            if len(cardsPerTransfer) != 0:
                print("\tAverage cards per transfer: " + str(format( (float (sum(cardsPerTransfer)) / len(cardsPerTransfer) ), "0.2f")))
            print("\tNumber of times each card changed hands: " + str(timesChangedHands))
            print("\nUser")
            print("\tCards held longest: " + str(playerCardsHeldLongest))
            print("\tCards guessed most: " + str(playerNumTimesGuessed))
            print("\tNumber of guesses: " + str(playerNumGuesses))
            print("\tNumber of correct guesses: " + str(playerNumCorrectGuesses))
            if playerNumGuesses != 0:
                print("\tPercent correct: " + str(format( (float(playerNumCorrectGuesses) / float(playerNumGuesses) * 100), "0.2f")) + "%")
            print("\nCPU")
            print("\tCards held longest: " + str(cpuCardsHeldLongest))
            print("\tCards guessed most: " + str(cpuNumTimesGuessed))

            print("\tNumber of guesses: " + str(cpuNumGuesses))
            print("\tNumber of correct guesses: " + str(cpuNumCorrectGuesses))

            # Print who won
            if cpuNumGuesses != 0:
                print("\tPercent correct: " + str(format( (float(cpuNumCorrectGuesses) / float(cpuNumGuesses) * 100 ), "0.2f") + "%"))
            if len(playerBooks) > len(cpuBooks):
                print("\n**** PLAYER WINS ****")
            elif len(cpuBooks) > len(playerBooks):
                print("**** CPU WINS *****")
            else:
                print("You tied, I guess?")
            playerHasWon = True
        turn += 1

# Parse a card to remove the suite

# Parse Card by Mike
def parseCardNum(card):
    if card.isdigit():
        return card
    parsedCard = ""
    for character in card:
        if character.isdigit() == True:
            parsedCard = parsedCard + character
        else:
            return parsedCard


# If the computer is in Smart mode (mode 2, where it guesses the rank of the last card drawn),
# This checks if that makes sense, basically whether CPU still has the card or if it has been put down 
# By david
def rankOKToGuess(requestedRank, hand, books, otherPlayerBooks):
    bookCounter = 0
    otherPlayerBookCounter = 0
    for book in books:
        if requestedRank in book[0]:
            bookCounter += 1
    for book in otherPlayerBooks:
        if requestedRank in book[0]:
            otherPlayerBookCounter += 1
    cardCounter = 0
    for card in hand:
        if requestedRank in card:
            cardCounter += 1
    if bookCounter == 4 or otherPlayerBookCounter == 4 or cardCounter == 4:
        return False
    rankFound = False
    for card in hand:
        if card in hand:
            rankFound = True
    return rankFound
    

# Draw a card into hand from deck
# By Felix
def drawCard(hand, deck):
    drawnCard = deck.pop(randrange(0, len(deck)))
    hand.append(drawnCard)
    return drawnCard


# Input/output formatting functions by David


def inputToCardNumber(inputCard): # If possible, simplify the user's input into a 1-13 card rank
    # Makes it so the user can enter ace, jack, queen, kings and guesses with s? on the end
    # for example, "kings?" becomes 13, "Aces" becomes 1, "6s" becomes 6, "1s?" becomes 1
    # No validation done here- just cleaning
    
    if inputCard.isdigit() or len(inputCard) == 0: # If it's just a number (or empty), skip all the other checks
        return inputCard

    # Trims s or ? off the end, in case the user enters "Got any 7s?" for example
    elif inputCard[-2].lower() == 's' and inputCard[-1] == '?':
        inputCard = inputCard[ : -2]
    elif inputCard[-1] == '?' or inputCard[-1].lower() == 's':
        inputCard = inputCard[ : -1]
    if inputCard.isdigit(): # If card is now just a number, return it 
        return inputCard

    else: # Input is not just a number, see if its one of the named card numbers
        inputCard = inputCard.lower()
        if inputCard == "ace":
            inputCard = "1"
        if inputCard == "jack":
            inputCard = "11"
        if inputCard == "queen":
            inputCard = "12"
        if inputCard == "king":
            inputCard = "13"
        return inputCard # If guess is invalid, just send it along anyway. inputCardRequest() will catch it
        
# Print all of the books passed, formatted for printing
def printBooks(books):
    printStr = "books: "
    for i in range(len(books)):
        printStr = printStr + formatCardForPrinting(books[i][0]) + 's'
        if i < (len(books) - 1):
            printStr = printStr + ', '
    return printStr

       
# Input player's guess
# By David
def inputCardRequest(yourHand):
    while True: # Repeat until there is valid input
        print("Your hand: " + formattedHandString(yourHand))
        print("Make a request of the other player")
        card = input("You: Got any ")
        #python does what?
        card = inputToCardNumber(card) # Format the user's input to 1 - 13 if possible
        # This part expects an int from 1-13
        try:
            card = int(card)
        except ValueError:
            print("\nInvalid card. Please enter a card rank ace through king (or 1-13)")
            continue
        else:
            if card < 1 or card > 13: # Player entered an invalid card number
                print("\nInvalid card. Please enter a card rank ace through king (or 1-13)")
                continue
            
            # Check if card entered is in user's hand
            cardInHand = False
            for handCard in yourHand:
                if str(card) == handCard[ : -1]:
                    cardInHand = True
            if cardInHand == False:
                print("\nInvalid card. You must enter a card you have in your hand.")
                continue
            
            # Otherwise, guess ok
            else:                             
                break
    return str(card)


# Turn cards from "13b" into King for printing to user, removes suite
def formatCardForPrinting(card):
    numberCard = parseCardNum(card)
    returnCard = ''
    if numberCard == '1':
        returnCard = "Ace"
    elif numberCard == '11':
        returnCard = "Jack"
    elif numberCard == "12":
        returnCard = "Queen"
    elif numberCard == '13':
        returnCard = "King"
    else:
        returnCard = numberCard
    return returnCard


# Recursively builds string to print a hand
def formattedHandString(hand, i = 0):
    if i == len(hand):
        return ""
    return formatCardForPrinting(hand[i]) + ' ' + formattedHandString(hand, i + 1)


# Check hand for any complete books, separate them from hand if any found
def separateBooks(hand, allBooks, userBooks): 
    handSet = set(hand)
    for book in allBooks:
        if set(book) <= handSet: # If every element of book is in hand
            print("Put down 4 " + formatCardForPrinting(book[0]) + "s")
            userBooks.append([]) # Make a new book
            for i in range(0, len(hand)): # Move cards from hand to new book
                if hand[i] in book:
                    userBooks[-1].append(hand[i]) # Append card to last book
            for card in book:
                if card in hand:
                    hand.remove(card)
                    


# Send guess to the other player, take any cards of that rank
# If go fish, return an empty list
def transferCardsFromPlayer(requestedRank, fromHand): 
    cardsToHandOver = []
    for card in fromHand: 
        if int(requestedRank) == int(card[:-1]): # If the card is of the requested rank
            cardsToHandOver.append(card)
    for card in cardsToHandOver:
        fromHand.remove(card)
    return cardsToHandOver



# Unit tests
def inputToCardNumberTest():
    while True:
        string = input("Enter a card to format: ")
        if string == "break":
            break
        print(inputToCardNumber(string))
        

def separateBooksTest():
    
    aces = ["1a", "1b", "1c", "1d"]
    twos = ["2a", "2b", "2c", "2d"]
    threes = ["3a", "3b", "3c", "3d"]
    fours = ["4a", "4b", "4c", "4d"]
    fives = ["5a", "5b", "5c", "5d"]
    sixes = ["6a", "6b", "6c", "6d"]
    sevens = ["7a", "7b", "7c", "7d"]
    eights = ["8a", "8b", "8c", "8d"]
    nines = ["9a", "9b", "9c", "9d"]
    tens = ["10a", "10b", "10c", "10d"]
    jacks = ["11a", "11b", "11c", "11d"]
    queens = ["12a", "12b", "12c", "12d"]
    kings = ["13a", "13b", "13c", "13d"]
    books = [aces, twos, threes, fours, fives, sixes, sevens, eights, nines, tens, jacks, queens, kings]
    hand = ["11a", "13d", "1a", "11b", "2a", "11c", "5c", "11d"]
    userBooks = []
    separateBooks(hand, books, userBooks)
    print(hand, userBooks)
   

main()
